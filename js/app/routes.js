define(['./app'], function(app) {
    app.config(routesConfig);

    routesConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routesConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('employee', {
                url: '/employee',
                template: '<ui-view></ui-view>',
            })
            .state('employee.list', {
                url: '/list',
                templateUrl: './modules/employeeListPage/page.html',
                controller: 'employeeListController',
                controllerAs: 'ctrl'
            })
            .state('employee.new', {
                url: '/new',
                templateUrl: './modules/employeeViewPage/page.html',
                controller: 'employeeViewController',
                controllerAs: 'ctrl'
            })
            .state('employee.edit', {
                url: '^/employee/id:int',
                templateUrl: './modules/employeeViewPage/page.html',
                controller: 'employeeViewController',
                controllerAs: 'ctrl'
            });

        $urlRouterProvider.otherwise('/employee/list');
    }
});