define([
    'app',
    './employeeApi/mock'    
], function (app) {
    app.run(['$httpBackend', function($httpBackend) {

        //CMS content
        $httpBackend.whenGET(/\.html$/).passThrough();
    }]);
});