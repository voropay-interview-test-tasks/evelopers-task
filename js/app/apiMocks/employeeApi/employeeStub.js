define(['angular'], function (angular) {

    function EmployeeStub() {
        this.lastId = 0;
        this.data = [];

        this.createEmployee({
            firstName: 'Albert',
            lastName: 'Einstein',
            seniority: 45,
            age: 137,
            address: '112 Mercer Street in Princeton, Mercer County, New Jersey, United States'
        });

        this.createEmployee({
            firstName: 'Stephen',
            lastName: 'Hawking',
            seniority: 40,
            age: 74,
            address: 'Centre for Mathematical Sciences, Wilberforce Road Cambridge CB3 0WA, United Kingdom'
        });

        this.createEmployee({
            firstName: 'Dmitri',
            lastName: 'Mendeleev',
            seniority: 40,
            age: 182,
            address: 'unknown'
        });
    }

    EmployeeStub.prototype.nextId = function () {
        return ++this.lastId;
    };

    EmployeeStub.prototype.getIndex = function (id) {
        var index;

        this.data.forEach(function (item, i) {
            !index && item.id == id && (index = i);
        });
        
        return index;
    };

    EmployeeStub.prototype.getEmployee = function (id) {
        var employee = angular.copy(
            this.data.filter(function (item) {
                return item.id == id;
            })[0]
        );

        employee.id = undefined;
        
        return employee;
    };

    EmployeeStub.prototype.createEmployee = function (employee) {
        this.data.push(employee);
        employee.id = this.nextId();
    };

    EmployeeStub.prototype.updateEmployee = function (id, employee) {
        employee.id = id;
        
        this.data[this.getIndex(id)] = employee;
    };

    EmployeeStub.prototype.deleteEmployee = function (id) {
        this.data.splice(this.getIndex(id), 1);
    };

    return new EmployeeStub();
});