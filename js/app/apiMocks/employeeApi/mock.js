define([
    'angular',
    'app',
    './employeeStub'
], function (angular, app, stub) {

    app.run(['$httpBackend', function($httpBackend) {
        $httpBackend.whenGET('rest/employee').respond(function() {
            return [200, stub.data];
        });

        $httpBackend.whenRoute('GET', 'rest/employee/:id')
            .respond(function(method, url, data, headers, params) {
                var employee = stub.getEmployee(+params.id);
                
                return [200, employee];
            });
        
        $httpBackend.whenRoute('PUT', 'rest/employee/:id')
            .respond(function(method, url, data, headers, params) {
                stub.updateEmployee(+params.id, angular.fromJson(data));

                return [200];
            });

        $httpBackend.whenPOST('rest/employee')
            .respond(function(method, url, data) {
                stub.createEmployee(angular.fromJson(data));

                return [200];
            });

        $httpBackend.whenRoute('DELETE', 'rest/employee/:id')
            .respond(function(method, url, data, headers, params) {
                stub.deleteEmployee(+params.id)

                return [200];
            });
    }]);
});