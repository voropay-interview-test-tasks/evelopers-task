define([
    'angular',
    'angularResource',
    'angularUIRouter',
    'angularMocks',
    'jquery',
    'bootstrap',

    'app',
    'routes',

    'modules/employeeApi/define',
    'modules/employeeListPage/define',
    'modules/employeeViewPage/define',
    
    'apiMocks/define'
], function(angular) {
    angular.bootstrap(document, ['app']);
});
