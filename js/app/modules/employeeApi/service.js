define(['app'], function (app) {
    app.service('employeeApiService', Service);

    Service.$inject = ['$resource'];

    function Service ($resource) {
        return $resource('rest/employee', {}, {
            loadList: {
                method: 'GET',
                isArray: true
            },
            load: {
                url: 'rest/employee/:id',
                method: 'GET'
            },
            save: {
                url: 'rest/employee/:id',
                method: 'PUT'
            },
            create: {
                method: 'POST'
            },
            delete: {
                url: 'rest/employee/:id',
                method: 'DELETE'
            }

        });
    }
});