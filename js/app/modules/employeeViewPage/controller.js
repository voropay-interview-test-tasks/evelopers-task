define([
    'angular',
    './module'
], function(angular, module) {
    Controller.$inject = ['$scope', '$state', '$stateParams', 'employeeApiService'];

    function Controller($scope, $state, $stateParams, employeeApiService) {
        var page = this;

        page.$scope = $scope;
        page.$state = $state;
        page.$stateParams = $stateParams;
        page.employeeApiService = employeeApiService;
        page.data = {};
        
        page.init();
    }

    Controller.prototype.init = function() {
        var page = this;
        
        page.data.employeeId = page.$stateParams.id;
        page.data.employee = {};
        
        page.loadEmployee();
    };
    
    Controller.prototype.loadEmployee = function () {
        var page = this;
        
        if (angular.isUndefined(page.data.employeeId)) {
            return;
        }

        page.data.employee = page.employeeApiService.load({'id': page.data.employeeId});
    };

    Controller.prototype.save = function() {
        var page = this;
        
        var method; 
        
        if (angular.isDefined(page.data.employeeId)) {
            method = page.employeeApiService.save;
        } else {
            method = page.employeeApiService.create;
        }

        method({'id': page.data.employeeId}, page.data.employee);
        
        page.$state.go('employee.list');
    };


    module.controller('employeeViewController', Controller);
});