define(['./module'], function(module) {
    Controller.$inject = ['$scope', 'employeeApiService'];

    function Controller($scope, employeeApiService) {
        var page = this;

        page.$scope = $scope;
        page.employeeApiService = employeeApiService;
        page.data = {};
        
        page.init();
    }

    Controller.prototype.init = function() {
        this.loadEmployees();
    };

    Controller.prototype.deleteEmployee = function(id) {
        this.employeeApiService.delete({'id': id});

        this.loadEmployees();
    };

    Controller.prototype.loadEmployees = function () {
        this.data.employees = this.employeeApiService.loadList();
    };

    module.controller('employeeListController', Controller);
});