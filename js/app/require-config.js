if (typeof define !== 'function') {
    // to be able to require file from node
    var define = require('amdefine')(module);
}

define({
    baseUrl: '/',
    paths: {
        jquery: 'components/jquery/dist/jquery',
        bootstrap: 'components/bootstrap/dist/js/bootstrap',
        angular: 'components/angular/angular',
        angularResource: 'components/angular-resource/angular-resource',
        angularUIRouter: 'components/angular-ui-router/release/angular-ui-router',
        angularMocks: 'components/angular-mocks/angular-mocks'
    },
    shim: {
        bootstrap: {
            deps: ['jquery']
        },
        angular: {
            exports: 'angular',
            deps: ['jquery']
        },
        angularResource: {
            deps: ['angular']
        },
        angularUIRouter: {
            deps: ['angular']
        },
        angularMocks: {
            deps: ['angular']
        }
    }
});
