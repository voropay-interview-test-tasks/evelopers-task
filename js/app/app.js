define(['angular'], function (angular) {
    return angular.module('app', [
        'ngResource',
        'ui.router',

        'employeeListPageModule',
        'employeeViewPageModule',

        'ngMockE2E'
    ]);
});