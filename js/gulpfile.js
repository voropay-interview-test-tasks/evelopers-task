var gulp = require('gulp'),
    bower = require('gulp-bower'),
    connect = require('gulp-connect');


//gulp.task('default', ['server', 'watch']);
gulp.task('default', ['server']);

gulp.task('server', function() {
  connect.server({
    port: 3003,
    root: 'app'
    //livereload: true
  });
});

/*
gulp.task('watch', function () {
  gulp.watch(['./app/!*.html'], ['html']);
  gulp.watch(['./app/!*.js'], ['js']);
  // gulp.watch(['./app/!*.css'], ['css']);
});

gulp.task('html', function () {
  gulp.src('./app/!*.html')
    .pipe(connect.reload());
});

gulp.task('js', function () {
  gulp.src('./app/!*.js')
      .pipe(connect.reload());
});
*/

/*
gulp.task('css', function () {
  gulp.src('./app/!*.css')
    .pipe(connect.reload());
});
*/

// load vendor dependencies 
gulp.task('bower', function() {
  return bower();
});

