package ru.evelopers.task;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by ostvpo on 6/28/16.
 */
@Configuration
@Import({ServiceConfig.class})
public class TestConfig {
}
