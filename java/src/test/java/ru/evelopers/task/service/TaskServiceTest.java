package ru.evelopers.task.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.evelopers.task.exception.TaskIsExecutingException;
import ru.evelopers.task.model.PersonResponse;

import java.util.List;

/**
 * Created by ostvpo on 6/28/16.
 */
public class TaskServiceTest extends BaseServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(TaskServiceTest.class);

    @Autowired
    private TaskService taskService;


    @Test(dataProvider = "testData"/*, threadPoolSize = 100, invocationCount = 10*/)
    public void test(Byte month, int resultLength, Long[] daysLeft) throws Exception {
        Integer id = taskService.createTask(month);

        List<PersonResponse> result;

        while (true) {
            try {
                result = taskService.getTaskResult(id);
                break;
            } catch (TaskIsExecutingException ex) {
                Thread.sleep(3000);
            }
        }

        Assert.assertEquals(result.size(), resultLength);
        Assert.assertNotNull(result);

        result.forEach(item -> logger.info(item.toString()));
    }
}
