package ru.evelopers.task.service;

import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.evelopers.task.model.PersonResponse;
import ru.evelopers.task.util.DateUtil;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

/**
 * author: ostvpo
 * date:  6/29/16 3:25 AM
 */

public class BirthServiceTest extends BaseServiceTest {

    @Autowired
    private BirthService birthService;

    @BeforeMethod
    public void initMocks() throws IOException {
        DateUtil spy = Mockito.spy(DateUtil.class);
        Mockito.when(spy.getDateNow()).thenReturn(LocalDate.of(2016, 6, 28));
        birthService.setDateUtil(spy);
    }

    @Test(dataProvider = "testData", threadPoolSize = 100, invocationCount = 10)
    public void test(Byte month, int resultLength, Long[] daysLeft) throws Exception {
        Future<List<PersonResponse>> persons = birthService.getPersons(month);

        while (!persons.isDone()) {
            Thread.sleep(3000);
        }

        List<PersonResponse> result = persons.get();
        Assert.assertEquals(resultLength, result.size());

        IntStream.range(0, daysLeft.length).forEachOrdered(i -> {
            Assert.assertEquals(daysLeft[i], result.get(i).getDaysLeft());
        });

        result.forEach(item -> logger.info(item.toString()));
    }


}
