package ru.evelopers.task.service;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import ru.evelopers.task.TestConfig;

/**
 * author: ostvpo
 * date:  6/29/16 3:26 AM
 */
@ContextConfiguration(classes = TestConfig.class)
public class BaseServiceTest extends AbstractTestNGSpringContextTests {

    @DataProvider(parallel = true)
    public static Object[][] testData() {

        return new Object[][] {
                {Byte.valueOf((byte) 10), 1, new Long[] {115l}},
                {Byte.valueOf((byte) 9), 1, new Long[] {86l}},
                {Byte.valueOf((byte) 8), 0, new Long[0]},
                {Byte.valueOf((byte) 7), 0, new Long[0]},
                {Byte.valueOf((byte) 6), 5, new Long[] {1l, 0l, 0l, 1l, 364l}},
                {Byte.valueOf((byte) 2), 2, new Long[] {236l, 225l}},
                {null, 5, new Long[] {1l, 0l, 0l, 1l, 364l}}
        };
    }

}
