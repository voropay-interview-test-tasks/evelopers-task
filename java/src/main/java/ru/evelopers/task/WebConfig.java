package ru.evelopers.task;

import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * author: ostvpo
 * date:  6/28/16 12:17 AM
 */

@Configuration
@Import(value = ServiceConfig.class)
@EnableWebMvc
@ComponentScan({
        "ru.evelopers.task.controller",
})
public class WebConfig {
}
