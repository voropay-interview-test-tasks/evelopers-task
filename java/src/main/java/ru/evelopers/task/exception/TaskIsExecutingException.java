package ru.evelopers.task.exception;

/**
 * author: ostvpo
 * date:  6/28/16 3:32 AM
 */

public class TaskIsExecutingException extends Exception {
    private Integer id;

    public TaskIsExecutingException(Integer id) {
        super();

        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
