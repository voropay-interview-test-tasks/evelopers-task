package ru.evelopers.task.exception;

/**
 * author: ostvpo
 * date:  6/29/16 5:26 AM
 */

public class TaskDoesNotExistException extends Exception {
    private Integer id;

    public TaskDoesNotExistException(Integer id) {
        super();

        this.id = id;
    }

    public Integer getId() {
        return id;
    }

}
