package ru.evelopers.task.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.evelopers.task.exception.TaskDoesNotExistException;
import ru.evelopers.task.exception.TaskIsExecutingException;
import ru.evelopers.task.model.PersonResponse;
import ru.evelopers.task.service.TaskService;

import java.util.List;

/**
 * author: ostvpo
 * date:  6/28/16 12:24 AM
 */

@RestController
@RequestMapping("task")
public class TaskController {
    private static final Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;


    @RequestMapping(value = "", method = RequestMethod.POST)
    public Object createTask(@RequestParam(required = false) Byte birthMonth) throws Exception {
        logger.info("Received request on task creation with birth month {}", birthMonth);
        Integer taskId = taskService.createTask(birthMonth);

        return new Object() {
            public Integer id = taskId;
        };
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public List<PersonResponse> getTask(@PathVariable("id") Integer id) throws Exception {
        logger.info("Received request on get task result with id {}", id);

        return taskService.getTaskResult(id);
    }

    @ResponseStatus(value= HttpStatus.OK)
    @ExceptionHandler(TaskIsExecutingException.class)
    public Object taskIsExecutingException(TaskIsExecutingException ex) {
        logger.info("Task with id {} is executing", ex.getId());

        return new Object() {
            public String msg = "task is executing";
        };
    }

    @ResponseStatus(value= HttpStatus.OK)
    @ExceptionHandler(TaskDoesNotExistException.class)
    public Object taskIsExecutingException(TaskDoesNotExistException ex) {
        logger.info("Task with id {} does not exist", ex.getId());

        return new Object() {
            public String msg = "Task does not exist";
        };
    }

}
