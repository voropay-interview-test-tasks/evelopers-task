package ru.evelopers.task.util;

import org.springframework.stereotype.Service;

/**
 * Created by ostvpo on 6/28/16.
 */
public class IdGenerator {

    private Object counterLock = new Object();
    private volatile int counter = 0;

    public int nextId() {
        synchronized (counterLock) {
            if (counter == Integer.MAX_VALUE) {
                counter = 0;
            }

            counter++;
        }

        return counter;
    }

}
