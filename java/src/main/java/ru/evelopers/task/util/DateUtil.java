package ru.evelopers.task.util;

import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * author: ostvpo
 * date:  6/29/16 4:49 AM
 */
public class DateUtil {

    public LocalDate getDateNow() {
        return LocalDate.now();
    }
}
