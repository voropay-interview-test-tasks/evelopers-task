package ru.evelopers.task.service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import ru.evelopers.task.model.Person;
import ru.evelopers.task.model.PersonResponse;
import ru.evelopers.task.util.DateUtil;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * author: ostvpo
 * date:  6/28/16 1:19 AM
 */

@Service
public class BirthService {
    private static final Logger logger = LoggerFactory.getLogger(BirthService.class);

    @Value("classpath:birthdays.json")
    private Resource birthdayResource;

    private DateUtil dateUtil;

    private List<Person> persons = null;

    private final static int MINUTE = 1000*60;


    @Async
    public Future<List<PersonResponse>> getPersons(Byte birthMonth) throws InterruptedException {
        LocalDate now = dateUtil.getDateNow();

        int month = Objects.isNull(birthMonth) ? now.getMonthValue() : birthMonth;

        logger.info("Requesting persons with birthday in {} month", birthMonth);

        List<PersonResponse> result = persons.stream()
                .filter(person -> person.getBirthday().getMonthValue() == month)
                .map(person -> {
                    LocalDate birthday = person.getBirthday();

                    if (now.compareTo(birthday) >= 0) {
                        birthday = birthday.withYear(now.getYear());
                    }

                    long daysLeft = now.until(birthday, ChronoUnit.DAYS);
                    daysLeft = (daysLeft < 0) ? now.until(birthday.plusYears(1), ChronoUnit.DAYS) : daysLeft;

                    return new PersonResponse(person.getName(), daysLeft);
                })
                .collect(Collectors.toList());

        sleep();

        return new AsyncResult<>(result);
    }

    void setDateUtil(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    @PostConstruct
    private void postConstruct() throws IOException {
        ObjectMapper mapper = initializeJacksonMapper();
        logger.info("Loading birthdays.json");
        persons = mapper.readValue(birthdayResource.getInputStream(), new TypeReference<List<Person>>() {});
        logger.info("Birthdays.json has been loaded ");

        dateUtil = new DateUtil();
    }

    private ObjectMapper initializeJacksonMapper() {
        logger.info("Initializing jackson mapper");

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, JsonParser.Feature.ALLOW_SINGLE_QUOTES);
        mapper.findAndRegisterModules();

        logger.info("Jackson mapper has been initialized");
        return mapper;
    }

    private void sleep() throws InterruptedException {
        int sleepTime = (new Random().nextInt(MINUTE)) + MINUTE;

        logger.info("sleep on {} ms", sleepTime);
        Thread.sleep(sleepTime);
        logger.info("wake up");
    }
}
