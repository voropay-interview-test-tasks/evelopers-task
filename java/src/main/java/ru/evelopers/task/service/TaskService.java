package ru.evelopers.task.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.evelopers.task.exception.TaskDoesNotExistException;
import ru.evelopers.task.exception.TaskIsExecutingException;
import ru.evelopers.task.model.PersonResponse;
import ru.evelopers.task.util.IdGenerator;

import javax.annotation.PostConstruct;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * author: ostvpo
 * date:  6/28/16 12:25 AM
 */

@Service
public class TaskService {
    private static final Logger logger = LoggerFactory.getLogger(TaskService.class);

    private Map<Integer, Future<List<PersonResponse>>> tasks;
    private IdGenerator generator;


    @Autowired
    private BirthService birthService;


    @PostConstruct
    private void postConstruct() throws NoSuchAlgorithmException {
        tasks = new HashMap<>();
        generator = new IdGenerator();
    }

    public Integer createTask(Byte birthMonth) throws Exception {
        logger.info("Creating task to get persons with birthday in {} month", birthMonth);

        int id = generator.nextId();
        tasks.put(Integer.valueOf(id), birthService.getPersons(birthMonth));

        logger.info("Created task with id {}", id);
        return id;
    }

    public List<PersonResponse> getTaskResult(Integer id) throws Exception {
        logger.info("Finding task result with id {}", id);

        Future<List<PersonResponse>> future;
        if ((future = tasks.get(id)) == null) {
            throw new TaskDoesNotExistException(id);
        }

        if (!future.isDone()) {
            throw new TaskIsExecutingException(id);
        }

        return future.get();
    }
}
