package ru.evelopers.task.model;

/**
 * author: ostvpo
 * date:  6/28/16 1:13 AM
 */

public class PersonResponse {
    private String name;
    private Long daysLeft;


    public PersonResponse() {
    }

    public PersonResponse(String name, Long daysLeft) {
        this.name = name;
        this.daysLeft = daysLeft;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(Long daysLeft) {
        this.daysLeft = daysLeft;
    }

    @Override
    public String toString() {
        return "PersonResponse{" +
                "name='" + name + '\'' +
                ", daysLeft=" + daysLeft +
                '}';
    }
}
