package ru.evelopers.task;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created by ostvpo on 6/28/16.
 */
@Configuration
@EnableAsync
@ComponentScan({
        "ru.evelopers.task.service"
})
public class ServiceConfig {
}
